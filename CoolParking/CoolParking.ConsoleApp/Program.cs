﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.ConsoleApp
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var timer = new WithdrawTimer();
            var a = new ParkingService(timer, new LogTimer(), new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log"));
            var b = new Parking(a);
            await b.Start();
        }
    }
}
