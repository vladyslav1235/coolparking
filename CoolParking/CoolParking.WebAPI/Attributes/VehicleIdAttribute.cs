﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Attributes
{
    
    public class VehicleIdAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            var sValue = value as string ?? string.Empty;

            return Regex.IsMatch(sValue, "[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
        }
    }
}
