﻿using System.IO;
using System.Reflection;

namespace CoolParking.WebAPI.Resources
{
    public static class CoolParkingConsts
    {
        public const string DefaultRoute = "api/";
        public static string LogPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
    }
}
