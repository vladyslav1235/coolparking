﻿using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Resources;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route(CoolParkingConsts.DefaultRoute + "[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            var balance = _parkingService.GetBalance();

            return Ok(balance);
        }
        
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            var capacity = _parkingService.GetCapacity();

            return Ok(capacity);
        }
        
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            var freePlaces = _parkingService.GetFreePlaces();

            return Ok(freePlaces);
        }
    }
}
