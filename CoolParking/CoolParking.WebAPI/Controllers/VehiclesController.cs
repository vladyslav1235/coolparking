﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Attributes;
using CoolParking.WebAPI.Resources;
using CoolParking.WebAPI.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route(CoolParkingConsts.DefaultRoute + "[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> GetVehicles()
        {
            var vehicles = _parkingService.GetVehicles();

            return Ok(vehicles);
        }
        
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<Vehicle>> GetVehicleById([FromRoute, VehicleId] string id)
        {
            var vehicles = _parkingService.GetVehicles();
            var vehicle = vehicles.FirstOrDefault(veh => veh.Id == id);
            if (vehicle is null)
            {
                NotFound($"Vehicle with id: {id} not found.");
            }

            return Ok(vehicle);
        }

        [HttpPost]
        public ActionResult AddVehicle([FromBody] AddVehicleRequest request)
        {
            var vehicle = Map(request);
            _parkingService.AddVehicle(vehicle);

            return new StatusCodeResult(StatusCodes.Status201Created);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle([FromRoute, VehicleId] string id)
        {
            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch
            {
                return NotFound();
            }

        }

        #region Mapping

        public static Vehicle Map(AddVehicleRequest request)
        {
            return new Vehicle(request.Id, request.VehicleType!.Value, request.Balance!.Value);
        }

        #endregion
    }
}
