﻿using System;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Attributes;
using CoolParking.WebAPI.Resources;
using CoolParking.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route(CoolParkingConsts.DefaultRoute + "[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public ActionResult GetLastTransaction()
        {
            var lastParkingTransactions = _parkingService.GetLastParkingTransactions();

            return Ok(lastParkingTransactions);
        }

        [HttpGet("all")]
        public ActionResult GetAllTransactions()
        {
            try
            {
                var logFromFile = _parkingService.ReadFromLog();
                return Ok(logFromFile);
            }
            catch (InvalidOperationException)
            {
                return NotFound("File not found");
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult TopUpVehicle([FromBody, VehicleId] TopUpVehicleBalanceRequest request)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == request.Id);
            if (vehicle is null)
            {
                return NotFound();
            }

            _parkingService.TopUpVehicle(request.Id, request.Sum!.Value);

            return Ok(vehicle);
        }
    }
}
