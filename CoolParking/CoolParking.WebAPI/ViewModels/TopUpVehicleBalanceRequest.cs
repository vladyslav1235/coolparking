﻿using System.ComponentModel.DataAnnotations;
using CoolParking.WebAPI.Attributes;

namespace CoolParking.WebAPI.ViewModels
{
    public record TopUpVehicleBalanceRequest
    {
        [Required]
        [VehicleId]
        public string Id { get; set; }

        [Required]
        public decimal? Sum { get; set; }

    }
}
