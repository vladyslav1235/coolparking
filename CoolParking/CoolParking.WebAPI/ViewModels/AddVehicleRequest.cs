﻿using System.ComponentModel.DataAnnotations;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Attributes;

namespace CoolParking.WebAPI.ViewModels
{
    public record AddVehicleRequest
    {
        [Required]
        [VehicleId]
        public string Id { get; set; }

        [EnumDataType(typeof(VehicleType))]
        [Required]
        public VehicleType? VehicleType { get; set; }

        [Required]
        public decimal? Balance { get; set; }
    }
}
