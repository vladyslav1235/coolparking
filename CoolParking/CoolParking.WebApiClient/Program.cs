﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.WebApiClient.Commands;

namespace CoolParking.WebApiClient
{
    internal class Program
    {
        private static ICommand[] _commands = 
        {
            new AddVehicleCommand(),
            new GetFreePlacesCommand(),
            new GetParkingBalanceCommand(),
            new RemoveVehicleCommand(),
            new TopUpVehicleBalanceCommand(),
            new ShowAllVehiclesCommand()
        };
        static async Task Main(string[] args)
        {
            var task = Task.Run(async () =>
            {
                while (true)
                {
                    Console.WriteLine("Enter number of command:");
                    for (var i = 0; i < _commands.Length; i++)
                    {
                        Console.WriteLine($"{i+1} " + _commands[i].Name);
                    }
                    Console.WriteLine("Press x to exit");

                    var commandNumber = Console.ReadLine();
                    Console.Clear();
                    if (commandNumber is "x")
                    {
                        break;
                    }

                    if (!int.TryParse(commandNumber, out var res) && res <= 0 || res > _commands.Length)
                    {
                        Console.WriteLine("Invalid number");
                        continue;
                    }

                    var command = _commands[res-1];
                    await command.ExecuteAsync();
                }
            });

            await task;
        }
    }
}
