﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.WebApiClient.Commands
{
    public class GetParkingBalanceCommand : ICommand
    {
        public string Name => "Balance";

        public GetParkingBalanceCommand()
        {
        }

        public async Task ExecuteAsync()
        {
            using (var httpClient = new HttpClient())
            {
                var responseMessage = await httpClient.GetAsync(new Uri("http://localhost:57646/api/parking/balance"));
                Console.WriteLine(await responseMessage.Content.ReadAsStringAsync());
            }
        }
    }
}
