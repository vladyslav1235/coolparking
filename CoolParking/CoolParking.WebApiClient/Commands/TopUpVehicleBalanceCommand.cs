﻿using System;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using CoolParking.WebApiClient.Models;
using Newtonsoft.Json;
using JsonConverter = System.Text.Json.Serialization.JsonConverter;

namespace CoolParking.WebApiClient.Commands
{
    public class TopUpVehicleBalanceCommand : ICommand
    {
        public string Name => "Top up";

        public TopUpVehicleBalanceCommand()
        {
        }

        public async Task ExecuteAsync()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Enter vehicle id");
                    var id = Console.ReadLine();
                    Console.WriteLine("Enter amount of money");
                    var amount = decimal.Parse(Console.ReadLine() ?? string.Empty);
                    var request = new TopUpVehicleBalanceRequest(){Id = id, Sum = amount};
                    using (var httpClient = new HttpClient())
                    {
                        var response = await httpClient.PutAsync(
                            new Uri("http://localhost:57646/api/transactions/topUpVehicle"), 
                            new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, MediaTypeNames.Application.Json));

                        var vehicle =
                            JsonConvert.DeserializeObject<Vehicle>(await response.Content.ReadAsStringAsync());
                        Console.WriteLine($"Id: {vehicle.Id} , balance: {vehicle.Balance}");
                        break;
                    }
                }
                catch
                {
                    Console.WriteLine("Some parameter is invalid.");
                    Console.WriteLine("Press x to exit.");
                    var button = Console.ReadKey();
                    if (button.KeyChar == 'x')
                    {
                        break;
                    }
                }
            }

        }
    }
}
