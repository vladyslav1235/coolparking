﻿using System.Threading.Tasks;

namespace CoolParking.WebApiClient.Commands
{
    public interface ICommand
    {
        public string Name { get; }

        public Task ExecuteAsync();
    }
}
