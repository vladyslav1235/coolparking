﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.WebApiClient.Commands
{
    public class RemoveVehicleCommand : ICommand
    {
        public string Name => "Remove";

        public RemoveVehicleCommand()
        {
        }
        public async Task ExecuteAsync()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Write vehicle id:");
                    var id = Console.ReadLine();
                    using (var httpClient = new HttpClient())
                    {
                        await httpClient.DeleteAsync(new Uri("http://localhost:57646/api/vehicles/" + id));
                        Console.WriteLine("Deleted");
                        break;
                    }
                }
                catch
                {
                    Console.WriteLine("Id is invalid");
                    Console.WriteLine("Press x to exit");
                    var button = Console.ReadKey();
                    if (button.KeyChar == 'x')
                    {
                        break;
                    }
                }
            }
        }
    }
}
