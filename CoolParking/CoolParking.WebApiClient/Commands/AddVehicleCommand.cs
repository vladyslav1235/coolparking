﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CoolParking.WebApiClient.Models;

namespace CoolParking.WebApiClient.Commands
{
    public class AddVehicleCommand : ICommand
    {
        public string Name => "Add";
        public AddVehicleCommand()
        {
        }

        public async Task ExecuteAsync()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Enter id:");
                    var id = Console.ReadLine();
                    Console.WriteLine("Enter type:");
                    var type = Enum.Parse<VehicleType>(Console.ReadLine() ?? string.Empty);
                    Console.WriteLine("Enter balance:");
                    var balance = decimal.Parse(Console.ReadLine() ?? string.Empty);
                    var vehicle = new AddVehicleRequest(id, type, balance);
                    using (var httpClient = new HttpClient())
                    {
                        await httpClient.PostAsync(new Uri("http://localhost:57646/api/vehicles"),
                            new StringContent(JsonSerializer.Serialize(vehicle), Encoding.UTF8, "application/json"));
                    }
                    break;
                }
                catch
                {
                    Console.WriteLine("Some parameter is invalid."); 
                    Console.WriteLine("Press x to exit");
                    var button = Console.ReadKey();
                    if (button.KeyChar == 'x')
                    {
                        break;
                    }
                }
            }
        }
    }
}
