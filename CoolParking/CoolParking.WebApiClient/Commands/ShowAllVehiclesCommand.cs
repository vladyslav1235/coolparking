﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.WebApiClient.Models;
using Newtonsoft.Json;

namespace CoolParking.WebApiClient.Commands
{
    public class ShowAllVehiclesCommand : ICommand
    {
        public string Name => "Get vehicles";

        public ShowAllVehiclesCommand()
        {
        }

        public async Task ExecuteAsync()
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(new Uri("http://localhost:57646/api/vehicles"));
                var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(await response.Content.ReadAsStringAsync()) ?? new List<Vehicle>();
                foreach (var vehicle in vehicles)
                {
                    Console.WriteLine($"Id: {vehicle.Id}, type: {vehicle.VehicleType}, balance: {vehicle.Balance}");
                }
            }
        }
    }
}
