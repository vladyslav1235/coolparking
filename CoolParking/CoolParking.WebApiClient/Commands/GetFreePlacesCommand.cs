﻿using System;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CoolParking.WebApiClient.Commands
{
    public class GetFreePlacesCommand : ICommand
    {
        public string Name => "Free places";

        public GetFreePlacesCommand()
        {
        }
        public async Task ExecuteAsync()
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(new Uri("http://localhost:57646/api/parking/freePlaces"));
                Console.WriteLine(await response.Content.ReadAsStringAsync());
            }
        }
    }
}
