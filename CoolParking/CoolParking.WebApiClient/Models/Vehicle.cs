﻿namespace CoolParking.WebApiClient.Models
{
    public class Vehicle
    {
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public Vehicle()
        {
            
        }
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; }
    }
}
