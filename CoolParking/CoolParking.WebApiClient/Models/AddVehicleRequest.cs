﻿namespace CoolParking.WebApiClient.Models
{
    public record AddVehicleRequest(string VehicleId, VehicleType? VehicleType, decimal? Balance)
    {
        public string VehicleId { get; set; } = VehicleId;

        public VehicleType? VehicleType { get; set; } = VehicleType;

        public decimal? Balance { get; set; } = Balance;
    }
}
