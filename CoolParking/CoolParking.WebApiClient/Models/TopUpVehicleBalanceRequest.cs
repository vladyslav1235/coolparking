﻿namespace CoolParking.WebApiClient.Models
{
    public record TopUpVehicleBalanceRequest
    {
        public string Id { get; set; }
        
        public decimal Sum { get; set; }
    }
}
