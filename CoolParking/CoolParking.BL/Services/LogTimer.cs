﻿// TODO: implement class LogTimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using CoolParking.BL.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class LogTimer : ITimerService
    {
        public event ElapsedEventHandler Elapsed;
        private bool _isStopped = false;
        public double Interval { get; set; } = Settings.TimerLog;
        public void Start()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    if (_isStopped)
                    {
                        break;
                    }

                    Thread.Sleep(Convert.ToInt32(Interval * 1000));
                    Elapsed?.Invoke(this, null);
                }
            });
        }

        public void Stop()
        {
            _isStopped = true;
        }

        public void Dispose()
        {
            _isStopped = true;
            if (Elapsed != null)
            {
                foreach (var d in Elapsed.GetInvocationList())
                {
                    Elapsed -= (ElapsedEventHandler)d;
                }
            }
        }
    }
}