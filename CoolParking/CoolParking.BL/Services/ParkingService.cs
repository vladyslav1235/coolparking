﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private static IList<Vehicle> _vehicles = new List<Vehicle>();
        private static IList<TransactionInfo> _transactionInfos = new List<TransactionInfo>();
        private const int MaxCapacity = Settings.ParkingMaxCapacity;
        private decimal _balance = Settings.Balance;
        private int _occupiedPlaces = 0;
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        public ParkingService(ITimerService withdrawWithdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawWithdrawTimer;
            _withdrawTimer.Start();
            _logTimer = logTimer;
            _logTimer.Start();
            _logService = logService;
            _withdrawTimer.Elapsed += Withdraw;
            _logTimer.Elapsed += Log;
        }

        private void Log(object sender, ElapsedEventArgs e)
        {
            var stringBuilder = new StringBuilder();
            var transactionInfos = _transactionInfos.ToList();
            _transactionInfos = new List<TransactionInfo>();

            foreach (var transaction in transactionInfos)
            {
                stringBuilder.AppendLine($"VehicleId: {transaction.VehicleId}, TransactionDate: {transaction.TransactionDate}, Sum: {transaction.Sum}");
            }
            _logService.Write(stringBuilder.ToString());
        }

        public decimal GetBalance()
        {
            return _balance;
        }

        public int GetCapacity()
        {
            return MaxCapacity;
        }

        public int GetFreePlaces()
        {
            return MaxCapacity - _occupiedPlaces;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_vehicles);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle is null)
            {
                throw new ArgumentNullException(nameof(vehicle), $"{nameof(vehicle)} can not be null.");
            }

            lock (_vehicles)
            {
                if (MaxCapacity <= _occupiedPlaces)
                {
                    throw new ArgumentException();
                }
                var isExist = _vehicles.Any(veh => veh.Id == vehicle.Id);
                if (isExist)
                {
                    throw new ArgumentException();
                }
                _vehicles.Add(vehicle);
                _occupiedPlaces++;
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (string.IsNullOrWhiteSpace(vehicleId))
            {
                throw new ArgumentNullException(nameof(vehicleId), $"{nameof(vehicleId)} can not be null.");
            }


            var vehicle = _vehicles.FirstOrDefault(veh => veh.Id.Equals(vehicleId));

            if (vehicle is null)
            {
                throw new ArgumentException($"Vehicle with id: {vehicleId} does not exist.");
            }

            lock (vehicle)
            {
                if (vehicle.Balance < 0)
                {
                    throw new InvalidOperationException();
                }
                _vehicles.Remove(vehicle);
                _occupiedPlaces--;
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (string.IsNullOrWhiteSpace(vehicleId))
            {
                throw new ArgumentNullException(nameof(vehicleId), $"{nameof(vehicleId)} can not be null.");
            }

            if (sum <= 0)
            {
                throw new ArgumentException();
            }

            var vehicle = _vehicles.FirstOrDefault(veh => veh.Id.Equals(vehicleId));

            if (vehicle is null)
            {
                throw new ArgumentException($"Vehicle with id: {vehicleId} does not exist.");
            }

            lock (vehicle)
            {
                vehicle.TopUpBalance(sum);
            }

        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionInfos.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        private void Withdraw(object sender, ElapsedEventArgs elapsed)
        {
            var vehicles = _vehicles.ToList();
            foreach (var vehicle in vehicles)
            {
                var transactionInfo = new TransactionInfo(vehicle.Id);
                lock (vehicle)
                {
                    var price = Settings.GetCoefficientByVehicleType(vehicle.VehicleType);
                    if (price > vehicle.Balance)
                    {
                        var totalPrice = vehicle.Balance + (price - vehicle.Balance) * Settings.FineRate;
                        vehicle.TakeOffFromBalance(totalPrice);
                        _balance += totalPrice;
                        transactionInfo.AddToSum(price);
                        continue;
                    }

                    vehicle.TakeOffFromBalance(price);
                    _balance += price;
                    transactionInfo.AddToSum(price);
                }

                _transactionInfos.Add(transactionInfo);
            }
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _vehicles = new List<Vehicle>();
            _transactionInfos = new List<TransactionInfo>();
        }
    }
}