﻿namespace CoolParking.BL.Interfaces
{
    public interface ICommand
    {
        public string Name { get; }

        public void Execute();
    }
}
