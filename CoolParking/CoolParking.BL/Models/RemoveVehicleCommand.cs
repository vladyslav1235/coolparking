﻿using System;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Models
{
    public class RemoveVehicleCommand : ICommand
    {
        private readonly IParkingService _parkingService;
        public string Name => "Remove";

        public RemoveVehicleCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        public void Execute()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Write vehicle id:");
                    var id = Console.ReadLine();
                    _parkingService.RemoveVehicle(id);
                }
                catch
                {
                    Console.WriteLine("Id is invalid");
                    Console.WriteLine("Press x to exit");
                    var button = Console.ReadKey();
                    if (button.KeyChar == 'x')
                    {
                        break;
                    }
                }
            }
        }
    }
}
