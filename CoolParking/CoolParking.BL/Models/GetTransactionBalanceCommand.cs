﻿using System;
using System.Linq;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Models
{
    public class GetTransactionBalanceCommand : ICommand
    {
        private readonly IParkingService _parkingService;
        public string Name => "Transaction balance";

        public GetTransactionBalanceCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        public void Execute()
        {
            var transaction = _parkingService.GetLastParkingTransactions();
            Console.WriteLine(transaction.Sum(x => x.Sum));
        }
    }
}
