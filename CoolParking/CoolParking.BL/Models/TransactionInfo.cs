﻿using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public string VehicleId { get; }
        public decimal Sum { get; private set; }
        public DateTimeOffset TransactionDate { get; } = DateTimeOffset.Now;

        public TransactionInfo(string vehicleId)
        {
            VehicleId = vehicleId;
        }

        public void AddToSum(decimal sum)
        {
            Sum += sum;
        }
    }
}