﻿using System;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Models
{
    public class ShowAllVehiclesCommand : ICommand
    {
        private readonly IParkingService _parkingService;
        public string Name => "Get vehicles";

        public ShowAllVehiclesCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            foreach (var vehicle in _parkingService.GetVehicles())
            {
                Console.WriteLine($"{vehicle.Id} {vehicle.VehicleType}");
            }
        }
    }
}
