﻿using CoolParking.BL.Interfaces;
using System;

namespace CoolParking.BL.Models
{
    public class GetFreePlacesCommand : ICommand
    {
        private readonly IParkingService _parkingService;
        public string Name => "Free places";

        public GetFreePlacesCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        public void Execute()
        {
            var freePlaces = _parkingService.GetFreePlaces();
            var totalPlaces = _parkingService.GetCapacity();
            Console.WriteLine($"{freePlaces} from {totalPlaces} are free.");
        }
    }
}
