﻿using System;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Models
{
    public class AddVehicleCommand : ICommand
    {
        public string Name => "Add";
        private readonly IParkingService _parkingService;
        public AddVehicleCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Enter id:");
                    var id = Console.ReadLine();
                    Console.WriteLine("Enter type:");
                    var type = Enum.Parse<VehicleType>(Console.ReadLine() ?? string.Empty);
                    Console.WriteLine("Enter balance:");
                    var balance = decimal.Parse(Console.ReadLine() ?? string.Empty);
                    var vehicle = new Vehicle(id, type, balance);
                    _parkingService.AddVehicle(vehicle);
                    break;
                }
                catch
                {
                    Console.WriteLine("Some parameter is invalid."); 
                    Console.WriteLine("Press x to exit");
                    var button = Console.ReadKey();
                    if (button.KeyChar == 'x')
                    {
                        break;
                    }
                }
            }
        }
    }
}
