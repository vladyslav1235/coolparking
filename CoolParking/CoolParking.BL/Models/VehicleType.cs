﻿namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        PassengerCar = 0,
        Truck,
        Bus,
        Motorcycle
    }
}