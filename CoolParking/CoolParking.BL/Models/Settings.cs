﻿using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal Balance = 0;
        public const int ParkingMaxCapacity = 10;
        public const int TimerWithdraw = 5;
        public const int TimerLog = 60;
        public const decimal FineRate = 2.5m;

        public static decimal GetCoefficientByVehicleType(VehicleType @vehicleType)
        {
            return @vehicleType switch
            {
                VehicleType.PassengerCar => 2.5m,
                VehicleType.Truck => 5m,
                VehicleType.Bus => 3.5m,
                VehicleType.Motorcycle => 1,
                _ => throw new ArgumentOutOfRangeException(nameof(vehicleType), @vehicleType, null)
            };
        }
    }
}
