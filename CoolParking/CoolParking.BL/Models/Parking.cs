﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private readonly ICommand[] _commands;

        public Parking(IParkingService parkingService)
        {
            _commands = new ICommand[]
            {
                new AddVehicleCommand(parkingService),
                new GetFreePlacesCommand(parkingService),
                new GetParkingBalanceCommand(parkingService),
                new RemoveVehicleCommand(parkingService),
                new TopUpVehicleBalanceCommand(parkingService),
                new ShowAllVehiclesCommand(parkingService),
                new GetTransactionBalanceCommand(parkingService)
            };
        }

        public Task Start()
        {
            var task = Task.Run(() =>
            {
                while (true)
                {
                    Console.WriteLine("Enter command:");
                    foreach (var command in _commands)
                    {
                        Console.WriteLine(command.Name);
                    }
                    Console.WriteLine("Press x to exit");

                    var commandName = Console.ReadLine();
                    if (commandName is "x")
                    {
                        break;
                    }
                    var c = _commands.FirstOrDefault(x => x.Name == commandName);
                    c?.Execute();
                }
            });

            return task;
        }
    }
}