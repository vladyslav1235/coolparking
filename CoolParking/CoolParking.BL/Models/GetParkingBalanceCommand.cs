﻿using CoolParking.BL.Interfaces;
using System;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class GetParkingBalanceCommand : ICommand
    {
        private readonly IParkingService _parkingService;
        public string Name => "Balance";

        public GetParkingBalanceCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            Console.WriteLine(_parkingService.GetBalance());
        }
    }
}
