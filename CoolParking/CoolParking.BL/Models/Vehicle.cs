﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly Random Rnd = new ();
        private const string AvailableChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            ValidateCtorParams(id, balance);
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; private set; }

        public void TopUpBalance(decimal value)
        {
            Balance += value;
        }

        public void TakeOffFromBalance(decimal value)
        {
            Balance -= value;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var plateNumber =
                $"{GenerateRandomChar()}{GenerateRandomChar()}-{Rnd.Next(0, 9999):D4}-{GenerateRandomChar()}{GenerateRandomChar()}";

            return plateNumber;
        }

        private static char GenerateRandomChar()
        {
            return AvailableChars[Rnd.Next(0, AvailableChars.Length)];
        }

        private static void ValidateCtorParams(string id, decimal balance)
        {
            if (!Regex.IsMatch(id, "[A-Z]{2}-[0-9]{4}-[A-Z]{2}") )
            {
                throw new ArgumentException(nameof(id));
            }
            
            if (balance < 0)
            {
                throw new ArgumentException(nameof(balance));
            }
        }
    }
}
