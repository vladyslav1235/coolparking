﻿using CoolParking.BL.Interfaces;
using System;

namespace CoolParking.BL.Models
{
    public class TopUpVehicleBalanceCommand : ICommand
    {
        private readonly IParkingService _parkingService;
        public string Name => "Top up";

        public TopUpVehicleBalanceCommand(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void Execute()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Enter vehicle id");
                    var id = Console.ReadLine();
                    Console.WriteLine("Enter amount of money");
                    var amount = decimal.Parse(Console.ReadLine() ?? string.Empty);
                    _parkingService.TopUpVehicle(id, amount);
                }
                catch
                {
                    Console.WriteLine("Some parameter is invalid.");
                    Console.WriteLine("Press x to exit.");
                    var button = Console.ReadKey();
                    if (button.KeyChar == 'x')
                    {
                        break;
                    }
                }
            }
            
        }
    }
}
